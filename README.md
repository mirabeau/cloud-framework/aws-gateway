AWS Gateway
=========
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Creates a gateway host with autoscaling group in your public subnet(s) with an elastic IP address that automatically re-attaches if the host is killed.
In addition this host will also update your private subnet's routing rules to act as a default gateway.
If you don't want that, you probably don't need this role, take a look at the `[aws-bastion](https://gitlab.com/mirabeau/cloud-framework/aws-bastion)` role instead.

This gateway host can be used in place of a bastion host but also routes internet traffic for your private subnets.

The flag `enable_ssh` can be disabled to use this purely as a (cheaper) NAT gateway that does not allow any SSH traffic.
(it can still be managed through Systems Manager sessions though)

Note that the managed AWS NAT Gateway has several advantages over this gateway host, so the only real reason to use this instead is to save cost.
AWS NAT Gateway provides (see https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html):
- better scaling (starts at 5GBit and scales up to 45GBit)
- redundancy
- higher cost (~35 USD per gateway, but AWS Suggests using 1 per AZ -> can be costly)

If you want your cluster to sleep during off-hours you can provide this role with `sandman_tags`, which will be applied to the provisioned AutoScaling Group.
For example:
```yaml
sandman_tags:
  mcf:scheduleDown    : "00 19 ? * MON-FRI *"
  mcf:scheduleUp      : "00 7 ? * MON-FRI *"
  mcf:scheduleTimeZone: "Europe/Amsterdam"
```
Will cause the ASG to shutdown at 19:00 during the week and start backup again at 07:00 during the week.
You can manually set the `mcf:skipSchedule` tag on an ASG to skip the sleep events (including wakeup!).
NOTE that it's also possible to set the `mcf:scheduleXXX` tags through `cloudformation_tags` but this will also include the tags on all other resources (such as the SecurityGroup), which pollutes resource tags and therefore is not recommended.

Resources created are:
* Elastic IP address
  * Policy that allows host to attach it
* Gateway host
  * Launch Configuration
  * Autoscaling Group
* IAM Role/Profile to read SSM parameters and let cfn-init and ssm-agent function
* Security Group
  * Ingress port 22 open if ssh_enabled is true
* Routing rules for private subnets

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  

Required python modules:
* boto
* boto3
* awscli
* docker

Dependencies
------------
* aws-setup
* aws-utils
* aws-lambda
* aws-vpc

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
```
Role Defaults
-------------
```yaml
create_changeset   : True
debug              : False
cloudformation_tags: {}
tag_prefix         : "mcf"
private_bucket_name: "{{ account_name }}-private"
private_bucket_region: "{{ aws_region }}"

aws_gateway_params:
  create_changeset: "{{ create_changeset }}"
  debug           : False

  ami             : "{{ amis['ec2_latest_v2_ami'] }}"
  instance_type   : "t3.nano"
  create_keypair  : True
  keypair_name    : "{{ environment_abbr }}-gateway"

  enable_mfa      : False
  enable_ssh      : True
  cfninit_bucket_name  : "{{ private_bucket_name }}"
  cfninit_bucket_region: "{{ private_bucket_region }}"
```
Example Playbooks
----------------
Deploy Gateway using custom encrypted AMI
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    environment_type: "test"
    environment_abbr: "tst"
    aws_utils_params:
      ec2_encrypt_ami: True

  pre_tasks:
    - name: Get latest AWS AMI's
      include_role:
        name: aws-utils
        tasks_from: get_aws_amis
  roles:
    - aws-setup
    - aws-lambda
    - aws-vpc
    - env-acl
    - aws-gateway
```

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
